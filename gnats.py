"""GNATS bug database to Bitbucket issue converter.

This Python 3 script reads a list of GNATS bug files from stdin and writes a
resulting Bitbucket issues database to stdout.

This script requires the dateutil (http://labix.org/python-dateutil) module.

---------------------------------------------------------------------------
Copyright (c) 2013 The Regents of the University of California.
All rights reserved.

Permission to use, copy, modify, and distribute this software and its
documentation for any purpose, without fee, and without written agreement is
hereby granted, provided that the above copyright notice and the following
two paragraphs appear in all copies of this software.

IN NO EVENT SHALL THE UNIVERSITY OF CALIFORNIA BE LIABLE TO ANY PARTY FOR
DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT
OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF THE UNIVERSITY OF
CALIFORNIA HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

THE UNIVERSITY OF CALIFORNIA SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS
ON AN "AS IS" BASIS, AND THE UNIVERSITY OF CALIFORNIA HAS NO OBLIGATION TO
PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
---------------------------------------------------------------------------
"""

import sys
from datetime import datetime
from tzinfo import tzd
from dateutil import parser as dateparser

SINGLE_LINE = 0
MULTI_LINE = 1

gnats_field_types = {'Number': SINGLE_LINE,
                     'Category': SINGLE_LINE,
                     'Synopsis': SINGLE_LINE,
                     'Confidential': SINGLE_LINE,
                     'Severity': SINGLE_LINE,
                     'Priority': SINGLE_LINE,
                     'Responsible': SINGLE_LINE,
                     'State': SINGLE_LINE,
                     'Class': SINGLE_LINE,
                     'Submitter-Id': SINGLE_LINE,
                     'Arrival-Date': SINGLE_LINE,
                     'Closed-Date': SINGLE_LINE,
                     'Last-Modified': SINGLE_LINE,
                     'Originator': SINGLE_LINE,
                     'Release': SINGLE_LINE,
                     'Organization': SINGLE_LINE,
                     'Environment': MULTI_LINE,
                     'Description': MULTI_LINE,
                     'How-To-Repeat': MULTI_LINE,
                     'Fix': MULTI_LINE,
                     'Release-Note': SINGLE_LINE,
                     'Audit-Trail': MULTI_LINE,
                     'Unformatted': SINGLE_LINE
}
field_map = {'component': 'Category',
             'title': 'Synopsis',
             'version': 'Release',
}
content_fields = ['Description',
                  'Environment',
                  'How-To-Repeat',
                  'Fix',
                  'Responsible',
                  'Originator',
                  'Confidential',
                  'Severity',
                  'Priority',
                  'State',
                  'Class',
                  'Submitter-Id',
                  'Arrival-Date',
                  'Closed-Date',
                  'Last-Modified']
date_map = {'created_on': 'Arrival-Date'}
gnats_severities = {'non-critical': 0, 'serious': 1, 'critical': 2}
gnats_priorities = {'low': 0, 'medium': 1, 'high': 2}
bb_priorities = ['minor', 'major', 'critical']
status_map = {'open': 'open',
              'analyzed': 'open',
              'suspended': 'on hold',
              'feedback': 'resolved',
              'closed': 'resolved'}
class_kind_map = {'sw-bug': 'bug',
                  'doc-bug': 'bug',
                  'support': 'task',
                  'change-request': 'proposal'}
class_status_map = {'mistaken': 'invalid',
                    'duplicate': 'duplicate'}

def stringify(value):
    if value is None:
        return 'null'
    elif isinstance(value, str):
        return '"' + value.replace('\\', '\\\\').replace('"', '\\"') + '"'
    else:
        return repr(value)

def pad(field):
    return ' ' * (14 - len(field))

def convert_date(date):
    date = date.strip()
    if not date:
        return None
    dt = dateparser.parse(date, tzinfos=tzd)
    return dt.isoformat()

def parse_field(line):
    if line.startswith('>') and ':' in line:
        field = line[1:line.index(':')]
        if field not in gnats_field_types:
            return None, None
        elif gnats_field_types[field] == SINGLE_LINE:
            value = line[line.index(':')+1:].strip()
            return field, value
        else:
            return field, ''
    else:
        return None, None

def add_field(fields, crnt_field, crnt_text):
    if crnt_field:
        fields[crnt_field] = crnt_text

def parse_log(line):
    if '-Changed-From' in line:
        idx = line.index('-Changed-From')
        field = line[0:idx]
        if field in gnats_field_types:
            assert gnats_field_types[field] == SINGLE_LINE
            return field
    if line.startswith('Comment-Added-By:'):
        return 'Comment'
    if line.startswith('From:'):
        return 'Email'
    return None

class Database:
    meta_data = {'"default_assignee"': 'null',
                 '"default_component"': 'null',
                 '"default_kind"': '"bug"',
                 '"default_milestone"': 'null',
                 '"default_version"': 'null'}
    def __init__(self):
        self.bugs = []
        self.comments = []
        self.logs = []
        self.components = set()
        self.versions = set()
    def parse_bug(self, lines):
        fields = {}
        crnt_field = None
        crnt_text = ''
        for line in lines:
            field, value = parse_field(line)
            if field:
                add_field(fields, crnt_field, crnt_text)
                crnt_field, crnt_text = field, value
            else:
                crnt_text += line
        add_field(fields, crnt_field, crnt_text)
        self.bugs.append(Bug(self, fields))
    def export(self):
        rep = '{\n  "issues": [\n'
        self.bugs.sort(key=lambda bug: bug.bb_fields['id'])
        rep += ',\n'.join(bug.export() for bug in self.bugs)
        rep += '\n  ],\n  "comments":[\n'
        rep += ',\n'.join(comment.export() for comment in self.comments)
        rep += '\n  ],\n  "logs":[\n'
        rep += ',\n'.join(log.export() for log in self.logs)
        rep += '\n  ],\n  "meta":{\n    '
        rep += ',\n    '.join(key + ': ' + self.meta_data[key]
                              for key in sorted(self.meta_data))
        rep += '\n  },\n  "components":[\n    '
        rep += ',\n    '.join('{\n      "name": "' + component + '"\n    }'
                              for component in sorted(self.components))
        rep += '\n  ],\n  "versions":[\n    '
        rep += ',\n    '.join('{\n      "name": "' + version + '"\n    }'
                              for version in sorted(self.versions))
        rep += '\n  ]\n}'
        return rep

class Entry:
    default_fields = {}
    def __init__(self):
        self.bb_fields = self.default_fields.copy()
    def export(self):
        rep = '    {\n      '
        sorted_fields = sorted(self.bb_fields)
        entries = (stringify(field) + ': ' + stringify(self.bb_fields[field])
                   for field in sorted_fields)
        rep += ',\n      '.join(entries)
        return rep + '\n    }'

class Bug(Entry):
    default_fields = {'assignee': None,
                      'component': None,
                      'content': None,
                      'edited_on': None,
                      'milestone': None,
                      'reporter': None,
                      'version': None,
                      'watchers': None
    }
    def __init__(self, database, fields):
        Entry.__init__(self)
        self.database = database
        self.gnats_fields = fields
        self.translate()
    def translate(self):
        for field in field_map:
            self.bb_fields[field] = self.gnats_fields[field_map[field]]
        for field in date_map:
            self.bb_fields[field] = convert_date(self.gnats_fields[date_map[field]])
        self.bb_fields['updated_on'] = datetime.now().isoformat()
        self.bb_fields['content_updated_on'] = self.bb_fields['updated_on']
        self.bb_fields['id'] = int(self.gnats_fields['Number'])
        self.bb_fields['status'] = status_map[self.gnats_fields['State']]
        cls = self.gnats_fields['Class']
        if cls in class_kind_map:
            self.bb_fields['kind'] = class_kind_map[cls]
        else:
            self.bb_fields['kind'] = 'bug'
            self.bb_fields['status'] = class_status_map[cls]
        priority = gnats_priorities[self.gnats_fields['Priority']]
        severity = gnats_severities[self.gnats_fields['Severity']]
        self.bb_fields['priority'] = bb_priorities[max(priority, severity)]
        content = '```'
        for field in content_fields:
            if field in self.gnats_fields:
                content += ('\n>' + field +
                            (':\n' if gnats_field_types[field] == MULTI_LINE
                             else ':' + pad(field)))
                content += self.gnats_fields[field]
        self.bb_fields['content'] = content + '\n```'
        self.database.components.add(self.bb_fields['component'])
        self.database.versions.add(self.bb_fields['version'])
        self.handle_audit_trail()
        self.handle_unformatted()
    def handle_audit_trail(self):
        if 'Audit-Trail' not in self.gnats_fields:
            return
        lines = self.gnats_fields['Audit-Trail'].splitlines(True)
        crnt_field = None
        crnt_text = ''
        crnt_date = None
        for line in lines:
            if crnt_field and not crnt_date:
                if '-When:' in line:
                    crnt_date = convert_date(line[line.index('-When:')+6:])
                elif line.startswith('Date:'):
                    crnt_date = convert_date(line[5:])
            else:
                field = parse_log(line)
                if field:
                    self.add_comment(crnt_field, crnt_date, crnt_text)
                    crnt_field = field
                    crnt_text = ''
                    crnt_date = None
            crnt_text += line
        self.add_comment(crnt_field, crnt_date, crnt_text)
    def add_comment(self, field, date, text):
        if field:
            if self.database.comments:
                last_comment = self.database.comments[-1]
                if (last_comment.bug == self and
                    (not date or date == last_comment.bb_fields['created_on'])):
                    last_comment.bb_fields['content'] += text
                    return
            if not date:
                date = self.bb_fields['updated_on']
            comment = Comment(self, text, date)
            self.database.comments.append(comment)
    def handle_unformatted(self):
        text = self.gnats_fields['Unformatted']
        if text.strip():
            text = '```\n>Unformatted:\n' + text.rstrip() + '\n```'
            timestamp = self.bb_fields['updated_on']
            self.database.comments.append(Comment(self, text, timestamp))

class Comment(Entry):
    default_fields = {'content': None,
                      'user': None
    }
    comment_id = 100000
    def __init__(self, bug, text, timestamp):
        Entry.__init__(self)
        self.bug = bug
        self.bb_fields['id'] = Comment.comment_id
        Comment.comment_id += 1
        self.bb_fields['issue'] = bug.bb_fields['id']
        self.bb_fields['created_on'] = timestamp
        self.bb_fields['updated_on'] = bug.bb_fields['updated_on']
        self.bb_fields['content'] = text
    def export(self):
        text = self.bb_fields['content']
        self.bb_fields['content'] = '```\n' + text.rstrip() + '\n```'
        result = Entry.export(self)
        self.bb_fields['content'] = text
        return result

def main(*args):
    if args and (args[0] == '-h' or args[0] == '--help'):
        print(main.help_msg)
        exit()
    buglist = sys.stdin.readlines()
    db = Database()
    for path in buglist:
        db.parse_bug(open(path.strip(), encoding='latin1').readlines())
    print(db.export())

main.help_msg = \
"""GNATS bug database to Bitbucket issue converter.

This Python 3 script reads a list of GNATS bug files from stdin and writes a
resulting Bitbucket issues database to stdout."""

if __name__ == '__main__':
    main(*sys.argv[1:])
